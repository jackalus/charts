# Helm Charts
This repository collects Jack's not official charts from their individual repos and automatically publish them to our Helm repo, located at [jackalus.gitlab.io/charts](https://jackalus.gitlab.io/charts/index.yaml).

The charts collected and published currently by this repository are the following:

| Chart name   | Status       | Remark
|--------------|--------------|----------|
| [auto-deploy-app](https://gitlab.com/jackalus/helm-charts/auto-deploy-app) | Forked | Adds functionality to GitLab's auto-deploy-app. |
